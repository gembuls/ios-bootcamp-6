//
//  UIColor+CustomColor.swift
//  NewsMVVM
//
//  Created by Muhammad Nurrahman on 23/12/22.
//

import UIKit

extension UIColor {
    static let AppNavBarColor = UIColor(red: 52/255, green:152/255, blue: 219/255, alpha: 1.0)
    static let AppNavBarTitle = UIColor(red: 223/255, green:230/255, blue: 233/255, alpha: 1.0)
    static let AppNavBarLargeTitleTextColor = UIColor(red: 45/255, green:52/255, blue: 54/255, alpha: 1.0)
}
